# Riotous Cars

A Free (as in Freedom) physical 2D racing game. Developed using [Rust](https://www.rust-lang.org/) language and [Bevy](https://bevyengine.org/) engine.

![A screenshot of game with debug info](./screenshot.png)

**Warning**: This game is in early development!

## Run

1. [install Rust language](https://www.rust-lang.org/learn/get-started)
2. Install OS dependencies:
 - [GNU/Linux](https://github.com/bevyengine/bevy/blob/main/docs/linux_dependencies.md)
 - Windows: Make sure to install [VS2019 build tools](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=BuildTools&rel=16)
 - MacOS: Install the Xcode command line tools with `xcode-select --install` or the [Xcode app](https://apps.apple.com/en/app/xcode/id497799835)
3. Clone the repository:
```bash
git clone https://codeberg.org/mskf1383/Riotous-Cars.git
```
4. Go to project directory:
```bash
cd Riotous-Cars/
```
5. Build and run it:
```bash
cargo run --release
```


## How to play

- W: Go forward
- S: Go backward
- A: Rotate left
- D: Rotate right
- Left shift: Activate jet engine
- Mouse left-click: Fire

## TODO

- Player car
  - [x] Movement
  - [x] Rotation
  - [x] Jet Engine
  - [x] Gun
- Limitations
  - [ ] Health
  - [x] Gun temperature
  - [x] Jet engine capacity
- [ ] Enemies
- [ ] Upgrades
- [ ] Levels

## License

[![GNU GPL v3](./LICENSE.png)](./LICENSE)
