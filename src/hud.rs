use crate::components::*;
use bevy::prelude::*;

pub fn spawn_gun_temperature_bar(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
) {
    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                color: Color::rgb_u8(226, 226, 226),
                custom_size: Some(Vec2::new(80.0, 80.0)),
                ..default()
            },
            transform: Transform::from_xyz(0.0, 0.0, 20.0),
            ..default()
        })
        .insert(Hud {
            x: -87.5,
            y: 720.0 / 2.0 - 25.0 - 80.0 / 2.0,
        });

    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                color: Color::rgb_u8(156, 139, 0),
                custom_size: Some(Vec2::new(80.0, 80.0)),
                ..default()
            },
            transform: Transform::from_xyz(0.0, 0.0, 21.0),
            ..default()
        })
        .insert(Hud {
            x: -87.5,
            y: 720.0 / 2.0 - 25.0 - 80.0 / 2.0,
        })
        .insert(GunTemperatureBar);

    commands
        .spawn_bundle(SpriteBundle {
            texture: asset_server.load("termometer.png"),
            transform: Transform::from_xyz(0.0, 0.0, 20.0),
            ..default()
        })
        .insert(Hud {
            x: -152.5,
            y: 720.0 / 2.0 - 25.0 - 80.0 / 2.0,
        });
}

pub fn update_gun_temperature_bar(
    gun: Query<&Gun>,
    mut gun_temperature_bar_sprite: Query<&mut Sprite, With<GunTemperatureBar>>,
) {
    let gun = gun.single();
    let mut gun_temperature_bar_sprite =
        gun_temperature_bar_sprite.single_mut();
    if gun.temperature == 1.0 {
        gun_temperature_bar_sprite.custom_size = Some(Vec2::new(0.0, 0.0));
    } else if gun.temperature >= 100.0 {
        gun_temperature_bar_sprite.custom_size = Some(Vec2::new(80.0, 80.0));
    } else {
        gun_temperature_bar_sprite.custom_size =
            Some(Vec2::new(gun.temperature * 0.8, gun.temperature * 0.8));
    }
}

pub fn spawn_health_bar(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
) {
    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                color: Color::rgb_u8(226, 226, 226),
                custom_size: Some(Vec2::new(200.0, 35.0)),
                ..default()
            },
            transform: Transform::from_xyz(0.0, 0.0, 20.0),
            ..default()
        })
        .insert(Hud {
            x: 125.0,
            y: 720.0 / 2.0 - 25.0 - 35.0 / 2.0,
        });

    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                color: Color::rgb_u8(255, 70, 71),
                custom_size: Some(Vec2::new(200.0, 35.0)),
                ..default()
            },
            transform: Transform::from_xyz(0.0, 0.0, 21.0),
            ..default()
        })
        .insert(Hud {
            x: 125.0,
            y: 720.0 / 2.0 - 25.0 - 35.0 / 2.0,
        });

    commands
        .spawn_bundle(SpriteBundle {
            texture: asset_server.load("heart.png"),
            transform: Transform::from_xyz(0.0, 0.0, 20.0),
            ..default()
        })
        .insert(Hud {
            x: -2.5,
            y: 720.0 / 2.0 - 25.0 - 35.0 / 2.0,
        });
}

pub fn spawn_jet_engine_capacity_bar(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
) {
    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                color: Color::rgb_u8(226, 226, 226),
                custom_size: Some(Vec2::new(200.0, 35.0)),
                ..default()
            },
            transform: Transform::from_xyz(0.0, 0.0, 20.0),
            ..default()
        })
        .insert(Hud {
            x: 125.0,
            y: 720.0 / 2.0 - 25.0 - 35.0 - 10.0 - 35.0 / 2.0,
        });

    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                color: Color::rgb_u8(176, 31, 227),
                custom_size: Some(Vec2::new(200.0, 35.0)),
                ..default()
            },
            transform: Transform::from_xyz(0.0, 0.0, 21.0),
            ..default()
        })
        .insert(Hud {
            x: 125.0,
            y: 720.0 / 2.0 - 25.0 - 35.0 - 10.0 - 35.0 / 2.0,
        })
        .insert(JetEngineCapacityBar);

    commands
        .spawn_bundle(SpriteBundle {
            texture: asset_server.load("rocket.png"),
            transform: Transform::from_xyz(0.0, 0.0, 20.0),
            ..default()
        })
        .insert(Hud {
            x: -2.5,
            y: 720.0 / 2.0 - 25.0 - 35.0 - 10.0 - 35.0 / 2.0,
        });
}

pub fn update_jet_engine_capacity_bar(
    jet_engine: Query<&JetEngine>,
    mut jet_engine_capacity_bar_sprite_and_hud: Query<
        (&mut Sprite, &mut Hud),
        With<JetEngineCapacityBar>,
    >,
) {
    let jet_engine = jet_engine.single();
    let (mut jet_engine_capacity_bar_sprite, mut jet_engine_capacity_bar_hud) =
        jet_engine_capacity_bar_sprite_and_hud.single_mut();
    jet_engine_capacity_bar_sprite.custom_size =
        Some(Vec2::new(jet_engine.capacity * 2.0, 35.0));
    jet_engine_capacity_bar_hud.x = 125.0 - 100.0 + jet_engine.capacity;
}
