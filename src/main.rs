use bevy::prelude::*;
use bevy_inspector_egui::WorldInspectorPlugin;
use bevy_rapier2d::prelude::*;

mod camera;
mod car;
mod components;
mod ground;
mod gun;
mod hud;
mod sky;

fn main() {
    App::new()
        .insert_resource(WindowDescriptor {
            title: "Riotous Cars".to_string(),
            width: 1280.0,
            height: 720.0,
            ..default()
        })
        .add_plugins(DefaultPlugins)
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(60.0))
        .add_plugin(WorldInspectorPlugin::new())
        .add_plugin(RapierDebugRenderPlugin::default())
        .insert_resource(RapierConfiguration::default())
        .add_startup_system(camera::spawn)
        .add_startup_system(sky::spawn)
        .add_startup_system(hud::spawn_gun_temperature_bar)
        .add_startup_system(hud::spawn_health_bar)
        .add_startup_system(hud::spawn_jet_engine_capacity_bar)
        .add_startup_system(ground::spawn)
        .add_startup_system(car::spawn)
        .add_system(camera::movement)
        .add_system(sky::movement)
        .add_system(hud::update_gun_temperature_bar)
        .add_system(hud::update_jet_engine_capacity_bar)
        .add_system(car::movement)
        .add_system(car::rotation)
        .add_system(car::jet)
        .add_system(gun::rotation)
        .add_system(gun::fire)
        .add_system(gun::despawn_bullets)
        .run();
}
