use crate::components::*;
use bevy::prelude::*;
use bevy_rapier2d::prelude::*;
use rand::Rng;

pub fn spawn(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut config: ResMut<RapierConfiguration>,
) {
    config.gravity = Vec2::new(0.0, -600.0);

    let mut depth = -500.0;
    let mut last_block: f32 = 0.0;

    for position in -21..2400 {
        let rand_num: i32 = rand::thread_rng().gen_range(-2..3);

        // spawn uphill
        if rand_num > 0 && last_block >= -5.0 && last_block <= 5.0 {
            match last_block.round() as i32 {
                -5 => spawn_grass_d5(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                -4 => spawn_grass_d4(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                -3 => spawn_grass_d3(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                -2 => spawn_grass_d2(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                -1 => spawn_grass_d1(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                0 => spawn_grass_0(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                1 => spawn_grass_u1(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                2 => spawn_grass_u2(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                3 => spawn_grass_u3(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                4 => spawn_grass_u4(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                5 => spawn_grass_u5(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                _ => {
                    println!("Faild to spawn uphill!");
                }
            }
            last_block += 0.5;

        // spwan downhill
        } else if rand_num < 0 && last_block >= -5.0 && last_block <= 5.0 {
            match last_block.round() as i32 {
                -5 => spawn_grass_d5(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                -4 => spawn_grass_d4(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                -3 => spawn_grass_d3(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                -2 => spawn_grass_d2(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                -1 => spawn_grass_d1(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                0 => spawn_grass_0(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                1 => spawn_grass_u1(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                2 => spawn_grass_u2(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                3 => spawn_grass_u3(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                4 => spawn_grass_u4(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                5 => spawn_grass_u5(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                _ => {
                    println!("Faild to spawn downhill!");
                }
            }
            last_block -= 0.5;

        // spawn flat
        } else {
            match last_block.round() as i32 {
                -6 => spawn_grass_d5(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                -5 => spawn_grass_d4(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                -4 => spawn_grass_d3(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                -3 => spawn_grass_d2(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                -2 => spawn_grass_d1(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                -1 => spawn_grass_0(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                0 => spawn_grass_0(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                1 => spawn_grass_0(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                2 => spawn_grass_u1(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                3 => spawn_grass_u2(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                4 => spawn_grass_u3(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                5 => spawn_grass_u4(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                6 => spawn_grass_u5(
                    &mut commands,
                    &asset_server,
                    position,
                    &mut depth,
                ),
                _ => {
                    println!("Faild to spawn flat!");
                }
            }

            if last_block.round() > 0.0 {
                last_block -= 0.5;
            } else if last_block.round() < 0.0 {
                last_block += 0.5;
            }
        }

        commands.spawn_bundle(SpriteBundle {
            texture: asset_server.load("grass/underground.png"),
            transform: Transform::from_translation(Vec3::new(
                position as f32 * 25.0,
                depth - 540.0 - 2.5 * last_block as f32,
                9.0,
            )),
            ..default()
        });
    }
}

fn spawn_grass_0(
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    position: i32,
    depth: &mut f32,
) {
    commands
        .spawn_bundle(SpriteBundle {
            texture: asset_server.load("grass/0.png"),
            transform: Transform {
                translation: Vec3::new(position as f32 * 25.0, *depth, 10.0),
                ..default()
            },
            ..default()
        })
        .insert(RigidBody::Fixed)
        .insert(
            Collider::convex_hull(&[
                Vec2::new(-12.5, 7.5),
                Vec2::new(12.5, 7.5),
                Vec2::new(12.5, -12.5),
                Vec2::new(-12.5, -12.5),
            ])
            .expect("hello"),
        )
        .insert(Friction {
            coefficient: 1.0,
            ..default()
        })
        .insert(Earth);

    *depth += 0.0;
}

fn spawn_grass_u1(
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    position: i32,
    depth: &mut f32,
) {
    commands
        .spawn_bundle(SpriteBundle {
            texture: asset_server.load("grass/u1.png"),
            transform: Transform {
                translation: Vec3::new(
                    position as f32 * 25.0,
                    *depth + 2.5,
                    10.0,
                ),
                ..default()
            },
            ..default()
        })
        .insert(RigidBody::Fixed)
        .insert(
            Collider::convex_hull(&[
                Vec2::new(-12.5, 5.0),
                Vec2::new(12.5, 10.0),
                Vec2::new(12.5, -10.0),
                Vec2::new(-12.5, -15.0),
            ])
            .expect("hello"),
        )
        .insert(Friction {
            coefficient: 1.0,
            ..default()
        })
        .insert(Earth);

    *depth += 5.0;
}

fn spawn_grass_u2(
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    position: i32,
    depth: &mut f32,
) {
    commands
        .spawn_bundle(SpriteBundle {
            texture: asset_server.load("grass/u2.png"),
            transform: Transform {
                translation: Vec3::new(
                    position as f32 * 25.0,
                    *depth + 5.0,
                    10.0,
                ),
                ..default()
            },
            ..default()
        })
        .insert(RigidBody::Fixed)
        .insert(
            Collider::convex_hull(&[
                Vec2::new(-12.5, 2.5),
                Vec2::new(12.5, 12.5),
                Vec2::new(12.5, -7.5),
                Vec2::new(-12.5, -17.5),
            ])
            .expect("hello"),
        )
        .insert(Friction {
            coefficient: 1.0,
            ..default()
        })
        .insert(Earth);

    *depth += 10.0;
}

fn spawn_grass_u3(
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    position: i32,
    depth: &mut f32,
) {
    commands
        .spawn_bundle(SpriteBundle {
            texture: asset_server.load("grass/u3.png"),
            transform: Transform {
                translation: Vec3::new(
                    position as f32 * 25.0,
                    *depth + 7.5,
                    10.0,
                ),
                ..default()
            },
            ..default()
        })
        .insert(RigidBody::Fixed)
        .insert(
            Collider::convex_hull(&[
                Vec2::new(-12.5, 0.0),
                Vec2::new(12.5, 15.0),
                Vec2::new(12.5, -5.0),
                Vec2::new(-12.5, -20.0),
            ])
            .expect("hello"),
        )
        .insert(Friction {
            coefficient: 1.0,
            ..default()
        })
        .insert(Earth);

    *depth += 15.0;
}

fn spawn_grass_u4(
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    position: i32,
    depth: &mut f32,
) {
    commands
        .spawn_bundle(SpriteBundle {
            texture: asset_server.load("grass/u4.png"),
            transform: Transform {
                translation: Vec3::new(
                    position as f32 * 25.0,
                    *depth + 10.0,
                    10.0,
                ),
                ..default()
            },
            ..default()
        })
        .insert(RigidBody::Fixed)
        .insert(
            Collider::convex_hull(&[
                Vec2::new(-12.5, -3.5),
                Vec2::new(12.5, 17.5),
                Vec2::new(12.5, -2.5),
                Vec2::new(-12.5, -22.5),
            ])
            .expect("hello"),
        )
        .insert(Friction {
            coefficient: 1.0,
            ..default()
        })
        .insert(Earth);

    *depth += 20.0;
}

fn spawn_grass_u5(
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    position: i32,
    depth: &mut f32,
) {
    commands
        .spawn_bundle(SpriteBundle {
            texture: asset_server.load("grass/u5.png"),
            transform: Transform {
                translation: Vec3::new(
                    position as f32 * 25.0,
                    *depth + 12.5,
                    10.0,
                ),
                ..default()
            },
            ..default()
        })
        .insert(RigidBody::Fixed)
        .insert(
            Collider::convex_hull(&[
                Vec2::new(-12.5, -5.0),
                Vec2::new(12.5, 20.0),
                Vec2::new(12.5, -0.0),
                Vec2::new(-12.5, -25.0),
            ])
            .expect("hello"),
        )
        .insert(Friction {
            coefficient: 1.0,
            ..default()
        })
        .insert(Earth);

    *depth += 25.0;
}

fn spawn_grass_d1(
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    position: i32,
    depth: &mut f32,
) {
    commands
        .spawn_bundle(SpriteBundle {
            texture: asset_server.load("grass/d1.png"),
            transform: Transform {
                translation: Vec3::new(
                    position as f32 * 25.0,
                    *depth - 2.5,
                    10.0,
                ),
                ..default()
            },
            ..default()
        })
        .insert(RigidBody::Fixed)
        .insert(
            Collider::convex_hull(&[
                Vec2::new(12.5, 5.0),
                Vec2::new(-12.5, 10.0),
                Vec2::new(-12.5, -10.0),
                Vec2::new(12.5, -15.0),
            ])
            .expect("hello"),
        )
        .insert(Friction {
            coefficient: 1.0,
            ..default()
        })
        .insert(Earth);

    *depth -= 5.0;
}

fn spawn_grass_d2(
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    position: i32,
    depth: &mut f32,
) {
    commands
        .spawn_bundle(SpriteBundle {
            texture: asset_server.load("grass/d2.png"),
            transform: Transform {
                translation: Vec3::new(
                    position as f32 * 25.0,
                    *depth - 5.0,
                    10.0,
                ),
                ..default()
            },
            ..default()
        })
        .insert(RigidBody::Fixed)
        .insert(
            Collider::convex_hull(&[
                Vec2::new(12.5, 2.5),
                Vec2::new(-12.5, 12.5),
                Vec2::new(-12.5, -7.5),
                Vec2::new(12.5, -17.5),
            ])
            .expect("hello"),
        )
        .insert(Friction {
            coefficient: 1.0,
            ..default()
        })
        .insert(Earth);

    *depth -= 10.0;
}

fn spawn_grass_d3(
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    position: i32,
    depth: &mut f32,
) {
    commands
        .spawn_bundle(SpriteBundle {
            texture: asset_server.load("grass/d3.png"),
            transform: Transform {
                translation: Vec3::new(
                    position as f32 * 25.0,
                    *depth - 7.5,
                    10.0,
                ),
                ..default()
            },
            ..default()
        })
        .insert(RigidBody::Fixed)
        .insert(
            Collider::convex_hull(&[
                Vec2::new(12.5, 0.0),
                Vec2::new(-12.5, 15.0),
                Vec2::new(-12.5, -5.0),
                Vec2::new(12.5, -20.0),
            ])
            .expect("hello"),
        )
        .insert(Friction {
            coefficient: 1.0,
            ..default()
        })
        .insert(Earth);

    *depth -= 15.0;
}

fn spawn_grass_d4(
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    position: i32,
    depth: &mut f32,
) {
    commands
        .spawn_bundle(SpriteBundle {
            texture: asset_server.load("grass/d4.png"),
            transform: Transform {
                translation: Vec3::new(
                    position as f32 * 25.0,
                    *depth - 10.0,
                    10.0,
                ),
                ..default()
            },
            ..default()
        })
        .insert(RigidBody::Fixed)
        .insert(
            Collider::convex_hull(&[
                Vec2::new(12.5, -3.5),
                Vec2::new(-12.5, 17.5),
                Vec2::new(-12.5, -2.5),
                Vec2::new(12.5, -22.5),
            ])
            .expect("hello"),
        )
        .insert(Friction {
            coefficient: 1.0,
            ..default()
        })
        .insert(Earth);

    *depth -= 20.0;
}

fn spawn_grass_d5(
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    position: i32,
    depth: &mut f32,
) {
    commands
        .spawn_bundle(SpriteBundle {
            texture: asset_server.load("grass/d5.png"),
            transform: Transform {
                translation: Vec3::new(
                    position as f32 * 25.0,
                    *depth - 12.5,
                    10.0,
                ),
                ..default()
            },
            ..default()
        })
        .insert(RigidBody::Fixed)
        .insert(
            Collider::convex_hull(&[
                Vec2::new(12.5, -5.0),
                Vec2::new(-12.5, 20.0),
                Vec2::new(-12.5, -0.0),
                Vec2::new(12.5, -25.0),
            ])
            .expect("hello"),
        )
        .insert(Friction {
            coefficient: 1.0,
            ..default()
        })
        .insert(Earth);

    *depth -= 25.0;
}
