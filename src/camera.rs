use crate::components::*;
use bevy::prelude::*;

pub fn spawn(mut commands: Commands) {
    commands.spawn_bundle(Camera2dBundle::default());
}

pub fn movement(
    car_transforms: Query<&Transform, With<Car>>,
    mut camera_transforms: Query<&mut Transform, (With<Camera>, Without<Car>)>,
    mut huds_and_their_transform: Query<
        (&Hud, &mut Transform),
        (With<Hud>, Without<Camera>, Without<Car>),
    >,
) {
    for car_transform in car_transforms.iter() {
        for mut camera_transform in camera_transforms.iter_mut() {
            camera_transform.translation.x = car_transform.translation.x;
            camera_transform.translation.y = car_transform.translation.y;
        }

        for (hud, mut hud_transform) in huds_and_their_transform.iter_mut() {
            hud_transform.translation.x = car_transform.translation.x + hud.x;
            hud_transform.translation.y = car_transform.translation.y + hud.y;
        }
    }
}
