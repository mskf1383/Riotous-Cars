use std::f32::consts::PI;

use crate::components::*;
use bevy::prelude::*;
use bevy_rapier2d::prelude::*;

pub fn rotation(
    windows: Res<Windows>,
    q_camera: Query<(&Camera, &GlobalTransform)>,
    mut gun_transform: Query<&mut Transform, With<Gun>>,
) {
    let mut gun_transform = gun_transform.single_mut();
    let (cursor_x, cursor_y) = cursor_position(windows, q_camera);
    let (gun_x, gun_y) =
        (gun_transform.translation.x, gun_transform.translation.y);
    let (delta_x, delta_y) = (cursor_x - gun_x, cursor_y - gun_y);
    let angle = (delta_y / delta_x).atan();

    if (cursor_x, cursor_y) != (0.0, 0.0) {
        if gun_x < cursor_x {
            gun_transform.rotation = Quat::from_axis_angle(Vec3::Z, angle);
        } else {
            gun_transform.rotation = Quat::from_axis_angle(Vec3::Z, angle + PI);
        }
    }
}

pub fn fire(
    windows: Res<Windows>,
    q_camera: Query<(&Camera, &GlobalTransform)>,
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut gun_and_its_transform: Query<(&mut Gun, &Transform), With<Gun>>,
    car_velocity: Query<&Velocity, (With<Car>, Without<Wheel>)>,
    mouse: Res<Input<MouseButton>>,
    time: Res<Time>,
) {
    let car_velocity = car_velocity.single();
    let (mut gun, gun_transform) = gun_and_its_transform.single_mut();
    let (cursor_x, cursor_y) = cursor_position(windows, q_camera);
    let (gun_x, gun_y) =
        (gun_transform.translation.x, gun_transform.translation.y);
    let (delta_x, delta_y) = (cursor_x - gun_x, cursor_y - gun_y);
    let angle = (delta_y / delta_x).atan();

    if mouse.pressed(MouseButton::Left)
        && time.time_since_startup().as_millis() - gun.last_fire.as_millis()
            >= 200
        && !gun.locked
    {
        if gun_x < cursor_x {
            commands
                .spawn_bundle(SpriteBundle {
                    texture: asset_server.load("bullet.png"),
                    transform: Transform::from_translation(Vec3::new(
                        gun_x, gun_y, 11.0,
                    ))
                    .with_rotation(Quat::from_rotation_z(angle)),
                    ..default()
                })
                .insert(RigidBody::KinematicVelocityBased)
                //.insert(Collider::ball(2.5))
                .insert(CollisionGroups::new(2, 1))
                .insert(Velocity::linear(Vec2::new(
                    2000.0 * angle.cos() + car_velocity.linvel.x,
                    2000.0 * angle.sin() + car_velocity.linvel.y,
                )))
                .insert(Bullet);
        } else {
            commands
                .spawn_bundle(SpriteBundle {
                    texture: asset_server.load("bullet.png"),
                    transform: Transform::from_translation(Vec3::new(
                        gun_x, gun_y, 11.0,
                    ))
                    .with_rotation(Quat::from_rotation_z(angle + PI)),
                    ..default()
                })
                .insert(RigidBody::KinematicVelocityBased)
                //.insert(Collider::ball(2.5))
                .insert(CollisionGroups::new(2, 1))
                .insert(Velocity::linear(Vec2::new(
                    -2000.0 * angle.cos() + car_velocity.linvel.x,
                    -2000.0 * angle.sin() + car_velocity.linvel.y,
                )))
                .insert(Bullet);
        }

        gun.last_fire = time.time_since_startup();
        gun.temperature *= 1.1;
    } else if time.time_since_startup().as_millis() - gun.last_fire.as_millis()
        >= 200
    {
        if gun.temperature >= 2.0 {
            gun.temperature -= 1.0;
        } else {
            gun.temperature = 1.0;
        }
    }

    if gun.temperature >= 100.0 {
        gun.lock();
    } else if gun.temperature == 1.0 {
        gun.unlock();
    }
}

pub fn despawn_bullets(
    mut commands: Commands,
    windows: Res<Windows>,
    camera_transform: Query<&Transform, With<Camera>>,
    bullets_id_and_transform: Query<(Entity, &Transform), With<Bullet>>,
) {
    let window = windows.get_primary().unwrap();
    let camera_transform = camera_transform.single();

    for (bullet_id, bullet_transform) in bullets_id_and_transform.iter() {
        if bullet_transform.translation.x
            > camera_transform.translation.x + window.width() / 2.0 + 100.0
            || bullet_transform.translation.x
                < camera_transform.translation.x - window.width() / 2.0 - 100.0
            || bullet_transform.translation.y
                > camera_transform.translation.y + window.height() / 2.0 + 100.0
            || bullet_transform.translation.y
                < camera_transform.translation.y - window.height() / 2.0 - 100.0
        {
            commands.entity(bullet_id).despawn();
        }
    }
}

fn cursor_position(
    windows: Res<Windows>,
    q_camera: Query<(&Camera, &GlobalTransform)>,
) -> (f32, f32) {
    // get the camera info and transform
    // assuming there is exactly one main camera entity, so query::single() is OK
    let (camera, camera_transform) = q_camera.single();

    // get the window that the camera is displaying to (or the primary window)
    let window = windows.get_primary().unwrap();

    // check if the cursor is inside the window and get its position
    if let Some(screen_pos) = window.cursor_position() {
        // get the size of the window
        let window_size =
            Vec2::new(window.width() as f32, window.height() as f32);

        // convert screen position [0..resolution] to ndc [-1..1] (gpu coordinates)
        let ndc = (screen_pos / window_size) * 2.0 - Vec2::ONE;

        // matrix for undoing the projection and camera transform
        let ndc_to_world = camera_transform.compute_matrix()
            * camera.projection_matrix().inverse();

        // use it to convert ndc to world-space coordinates
        let world_pos = ndc_to_world.project_point3(ndc.extend(-1.0));

        // reduce it to a 2D value
        let world_pos: Vec2 = world_pos.truncate();

        (world_pos.x, world_pos.y)
    } else {
        (0.0, 0.0)
    }
}
