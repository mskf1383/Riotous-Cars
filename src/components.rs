use bevy::prelude::Component;
use std::time::Duration;

#[derive(Component)]
pub struct Car;

#[derive(Component)]
pub struct Wheel;

#[derive(Component)]
pub struct Earth;

#[derive(Component)]
pub struct Sky;

#[derive(Component)]
pub struct Gun {
    pub last_fire: Duration,
    pub temperature: f32,
    pub locked: bool,
}
impl Gun {
    pub fn lock(&mut self) {
        self.locked = true;
    }
    pub fn unlock(&mut self) {
        self.locked = false;
    }
}

#[derive(Component)]
pub struct Bullet;

#[derive(Component)]
pub struct JetEngine {
    pub capacity: f32,
}

#[derive(Component)]
pub struct Hud {
    pub x: f32,
    pub y: f32,
}

#[derive(Component)]
pub struct GunTemperatureBar;

#[derive(Component)]
pub struct JetEngineCapacityBar;
