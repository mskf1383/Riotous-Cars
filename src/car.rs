use crate::components::*;
use bevy::prelude::*;
use bevy_rapier2d::prelude::*;
use std::time::Duration;

pub struct CarSprite {
    normal: Handle<Image>,
    jet: Handle<Image>,
}

pub fn spawn(mut commands: Commands, asset_server: Res<AssetServer>) {
    // Spawn car body
    let car = commands
        .spawn_bundle(SpriteBundle {
            texture: asset_server.load("car.png"),
            ..default()
        })
        .insert(RigidBody::Dynamic)
        .insert(
            Collider::convex_hull(&[
                Vec2::new(60.0, 5.0),
                Vec2::new(145.0, 0.0),
                Vec2::new(145.0, -70.0),
                Vec2::new(135.0, -75.0),
                Vec2::new(-130.0, -75.0),
                Vec2::new(-140.0, -65.0),
                Vec2::new(-140.0, 5.0),
            ])
            .expect("Faild to spawn car body collider!"),
        )
        .with_children(|colliders| {
            colliders.spawn().insert(
                Collider::convex_hull(&[
                    Vec2::new(55.0, 5.0),
                    Vec2::new(5.0, 45.0),
                    Vec2::new(-65.0, 45.0),
                    Vec2::new(-105.0, 5.0),
                ])
                .expect("Faild to spawn car body collider!"),
            );
        })
        .insert(ColliderMassProperties::Mass(1500.0))
        .insert(Velocity::default())
        .insert(ExternalForce::default())
        .insert(ExternalImpulse::default())
        .insert(ReadMassProperties::default())
        .insert(Damping {
            linear_damping: 0.1,
            angular_damping: 0.1,
        })
        .insert(Friction {
            coefficient: 0.0,
            ..default()
        })
        .insert(CollisionGroups::new(2, 1))
        .insert(Car)
        .insert(JetEngine { capacity: 100.0 })
        .id();

    // Spawn joints
    let right_wheel_joint = RevoluteJointBuilder::new()
        .local_anchor1(Vec2::new(90.0, -80.0))
        .local_anchor2(Vec2::new(0.0, 0.0));

    let left_wheel_joint = RevoluteJointBuilder::new()
        .local_anchor1(Vec2::new(-85.0, -80.0))
        .local_anchor2(Vec2::new(0.0, 0.0));

    let gun_joint = RevoluteJointBuilder::new()
        .local_anchor1(Vec2::new(-32.5, 67.5))
        .local_anchor2(Vec2::new(0.0, 0.0));

    // Spawn wheels
    commands
        .spawn_bundle(SpriteBundle {
            texture: asset_server.load("wheel.png"),
            transform: Transform {
                translation: Vec3::new(0.0, 0.0, 2.0),
                ..default()
            },
            ..default()
        })
        .insert(RigidBody::Dynamic)
        .insert(Collider::ball(35.5))
        .insert(ColliderMassProperties::Mass(100.0))
        .insert(ImpulseJoint::new(car, right_wheel_joint))
        .insert(Velocity::default())
        .insert(ExternalForce::default())
        .insert(Damping {
            linear_damping: 0.1,
            angular_damping: 0.1,
        })
        .insert(Friction {
            coefficient: 30.0,
            ..default()
        })
        .insert(CollisionGroups::new(2, 1))
        .insert(Wheel);

    commands
        .spawn_bundle(SpriteBundle {
            texture: asset_server.load("wheel.png"),
            transform: Transform {
                translation: Vec3::new(0.0, 0.0, 2.0),
                ..default()
            },
            ..default()
        })
        .insert(RigidBody::Dynamic)
        .insert(Collider::ball(35.5))
        .insert(ColliderMassProperties::Mass(100.0))
        .insert(ImpulseJoint::new(car, left_wheel_joint))
        .insert(Velocity::default())
        .insert(ExternalForce::default())
        .insert(Damping {
            linear_damping: 0.1,
            angular_damping: 0.1,
        })
        .insert(Friction {
            coefficient: 50.0,
            ..default()
        })
        .insert(CollisionGroups::new(2, 1))
        .insert(Wheel);

    // Spawn gun
    commands
        .spawn_bundle(SpriteBundle {
            texture: asset_server.load("gun.png"),
            transform: Transform::from_translation(Vec3::new(0.0, 0.0, 12.0)),
            ..default()
        })
        .insert(RigidBody::Dynamic)
        .insert(Collider::ball(2.5))
        .insert(ImpulseJoint::new(car, gun_joint))
        .insert(CollisionGroups::new(10, 10))
        .insert(LockedAxes::ROTATION_LOCKED_Z)
        .insert(Gun {
            last_fire: Duration::new(0, 0),
            temperature: 1.0,
            locked: false,
        });

    // Insert car sprite resource
    commands.insert_resource(CarSprite {
        normal: asset_server.load("car.png"),
        jet: asset_server.load("car-jet-propulsion.png"),
    });
}

pub fn movement(
    mut wheels_velocity_and_external_force: Query<
        (&mut Velocity, &mut ExternalForce),
        With<Wheel>,
    >,
    keyboard: Res<Input<KeyCode>>,
) {
    for (mut wheel_velocity, mut wheel_external_force) in
        wheels_velocity_and_external_force.iter_mut()
    {
        if keyboard.pressed(KeyCode::W) && wheel_velocity.angvel > -50.0 {
            wheel_external_force.torque = -5000.0;
        } else if keyboard.pressed(KeyCode::S) && wheel_velocity.angvel < 50.0 {
            wheel_external_force.torque = 5000.0;
        } else if keyboard.pressed(KeyCode::Space) {
            wheel_velocity.angvel = 0.0;
        } else {
            wheel_external_force.torque = 0.0;
        }
    }
}

pub fn rotation(
    mut car_velocity_and_external_impulse: Query<
        (&Velocity, &mut ExternalImpulse),
        (With<Car>, Without<Wheel>),
    >,
    keyboard: Res<Input<KeyCode>>,
) {
    let (car_velocity, mut car_external_impulse) =
        car_velocity_and_external_impulse.single_mut();

    if keyboard.pressed(KeyCode::D) && car_velocity.angvel > -1.0 {
        car_external_impulse.torque_impulse = -1700.0;
    } else if keyboard.pressed(KeyCode::A) && car_velocity.angvel < 1.0 {
        car_external_impulse.torque_impulse = 1700.0;
    } else {
        car_external_impulse.torque_impulse = 0.0;
    }
}

pub fn jet(
    mut car_external_impulse_and_transform_and_texture: Query<
        (&mut ExternalImpulse, &Transform, &mut Handle<Image>),
        (With<Car>, Without<Wheel>),
    >,
    mut jet_engine: Query<&mut JetEngine>,
    car_sprite: Res<CarSprite>,
    keyboard: Res<Input<KeyCode>>,
) {
    let (mut car_external_impulse, car_trasform, mut car_texture) =
        car_external_impulse_and_transform_and_texture.single_mut();
    let mut jet_engine = jet_engine.single_mut();

    if keyboard.pressed(KeyCode::LShift) && jet_engine.capacity >= 1.0 {
        car_external_impulse.impulse = Vec2::new(
            15000.0 * car_trasform.rotation.to_scaled_axis().z.cos(),
            15000.0 * car_trasform.rotation.to_scaled_axis().z.sin(),
        );
        *car_texture = car_sprite.jet.clone();

        jet_engine.capacity -= 1.0;
    } else {
        car_external_impulse.impulse = Vec2::new(0.0, 0.0);
        *car_texture = car_sprite.normal.clone();

        if jet_engine.capacity < 100.0 {
            jet_engine.capacity += 0.05;
        }
    }
}
