use crate::components::*;
use bevy::prelude::*;

pub fn spawn(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands
        .spawn_bundle(SpriteBundle {
            texture: asset_server.load("sky.png"),
            ..default()
        })
        .insert(Sky);
}

pub fn movement(
    car_transforms: Query<&Transform, With<Car>>,
    mut sky_transforms: Query<&mut Transform, (With<Sky>, Without<Car>)>,
) {
    for car_transform in car_transforms.iter() {
        for mut sky_transform in sky_transforms.iter_mut() {
            sky_transform.translation.x = car_transform.translation.x;
            sky_transform.translation.y = car_transform.translation.y;
        }
    }
}
